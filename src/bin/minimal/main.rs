#![deny(unsafe_code)]
// #![deny(warnings)]
#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

use panic_probe as _;
use defmt_rtt as _;
use rtic::app;
// use rtic_monotonics::systick::*;
use stm32h7xx_hal::gpio::{Output, PushPull, PA6};
use stm32h7xx_hal::prelude::*;
// use stm32h7xx_hal;
use defmt::info;

use rtic_monotonics::stm32::Tim3 as Mono;
// use rtic_monotonics::stm32::*;
// use rtic_monotonics::Monotonic;

#[app(device = stm32h7xx_hal::pac, peripherals = true, dispatchers = [SPI1])]
mod app {
    use super::*;

    #[shared]
    struct Shared {}

    #[local]
    struct Local {
        led: PA6<Output<PushPull>>,
        state: bool,
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local) {
        // Setup clocks
        // let mut flash = cx.device.FLASH.constrain();
        let pwr = cx.device.PWR.constrain();
        let pwrcfg = pwr.ldo().vos3().freeze();
        let rcc = cx.device.RCC.constrain();

        // Initialize the systick interrupt & obtain the token to prove that we did
        
        // rtt_yinit_print!();
        info!("init");

        let clocks = rcc
            .use_hse(26.MHz())
            .sys_ck(500.MHz())
            .hclk(250.MHz())
            .pclk1(100.MHz())
            .pclk2(100.MHz())
            .pclk3(100.MHz())
            .pclk4(100.MHz())
            .freeze(pwrcfg, &cx.device.SYSCFG);

        let mono_token = rtic_monotonics::create_stm32_tim3_monotonic_token!();
        Mono::start(clocks.clocks.timx_ker_ck().to_Hz(), mono_token); // default STM32F303 clock-rate is 36MHz
        // Setup LED
        let gpioa = cx.device.GPIOA.split(clocks.peripheral.GPIOA);
        let mut led = gpioa
            .pa6
            .into_push_pull_output();
        led.set_high();

        // Schedule the blinking task
        blink::spawn().ok();

        (Shared {}, Local { led, state: false })
    }

    #[task(local = [led, state])]
    async fn blink(cx: blink::Context) {
        loop {
            info!("blink");
            if *cx.local.state {
                cx.local.led.set_high();
                *cx.local.state = false;
            } else {
                cx.local.led.set_low();
                *cx.local.state = true;
            }
            Mono::delay(rtic_monotonics::stm32::ExtU64::millis(1000)).await;
        }
    }
}